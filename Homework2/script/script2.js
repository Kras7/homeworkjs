'use strict';

let m;
let n;
let c=null;

do {
    m = +prompt('Enter first number: ');
} while ((m % 1)>0 ||(m === "")|| (m === null) || (isNaN(m)===true) ||(Boolean(m)===false));
do {
    n = +prompt('Enter second number: ');
} while ((n % 1)>0 || (n === "") || (n === null) || (isNaN(n)===true) || (Boolean(n)===false));
if (m>n) {
    [n, m] = [m, n];
}

for (c=m; c<=n; c++) {
    if ((c % 2)>0 && (c % 3)>0 && (c % 5)>0 && (c % 7)>0 && (c % 9)>0 && (c!=1) || (c === 2) || (c === 3) || (c === 5) || (c === 7)) {
        console.log(c);
    }
}

